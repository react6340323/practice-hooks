import movie from 'store/reducers/movie';
import client from './client';

export const getMovies = async (category) => {
  const { data } = await client.get('movies', {
    params: { filter: { where: { category } } },
  });
  return data;
};

export const getTrailer = async (movieId) => {
  try {
    const data = await client.get(`trailer/${movieId}`)
    return data;
  } catch(err) {
    return
  }
}
