import 'styles/globals.css';
import { Provider } from 'react-redux';
import store from 'store';
import Head from 'next/head'
import Header from 'components/layout/Header';

const MyApp = ({ Component, pageProps }) => {
  return (
    <Provider store={store}>
      <Head>
        <link rel='icon' href='/favicon.png'/>
      </Head>
      <Header />
      <Component {...pageProps} />
    </Provider>
  );
};

export default MyApp;
