import Head from 'next/head'
import Banner from 'components/layout/Banner';
import {getProps} from '../../utils/getServerSideProps'

const Latest = ({  
  trendingNow,
}) => {
  return (
    <div
      className={`relative h-screen lg:h-[140vh]`} /*bg-gradient-to-b from-gray-900/10 to-[#010511] */
    >
      <Head>
        <title>Latest - XStream</title>
      </Head>

      <main className="relative pb-12 lg:space-y-10">
        <Banner movies={trendingNow} />
      </main>
    </div>
  )
}

export const getServerSideProps = async () => {
  return getProps();
}

export default Latest
