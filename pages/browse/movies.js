import Head from 'next/head'
import Banner from 'components/layout/Banner';
import Slider from 'components/layout/Slider';
import {getProps} from '../../utils/getServerSideProps'

const Movies = ({
  netflixOriginals,
  actionMovies,
  comedyMovies,
  documentaries,
  horrorMovies,
  romanceMovies,
  topRated,
  trendingNow,
}) => {
  return (
    <div
      className={`relative h-screen lg:h-[140vh]`} /*bg-gradient-to-b from-gray-900/10 to-[#010511] */
    >
      <Head>
        <title>Movies - XStream</title>
      </Head>

      <main className="relative pb-12 lg:space-y-10">
        <Banner movies={netflixOriginals} />

        <section>
          <Slider title="Trending Now" movies={trendingNow} />
          <Slider title="Top Rated" movies={topRated} />
          <Slider title="Action Thrillers" movies={actionMovies} />
          <Slider title="Comedy Movies" movies={comedyMovies} />
          <Slider title="Horror Movies" movies={horrorMovies} />
          <Slider title="Romance Movies" movies={romanceMovies} />
          <Slider title="Documentaries" movies={documentaries} />
        </section>
      </main>
    </div>
  )
}

export const getServerSideProps = async () => {
  return getProps();
}

export default Movies
