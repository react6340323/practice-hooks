import { createSlice } from '@reduxjs/toolkit';

const initialState = {
  isOpen: false,
  movie: {}
};

export const previewSlice = createSlice({
  name: 'preview',
  initialState,
  reducers: {
    setIsOpen: (state) => {
        state.isOpen = !state.isOpen;
    },
    setMovie: (state, { payload }) => {
        state.movie = payload;
    },
  },
});

export const { setIsOpen, setMovie } = previewSlice.actions;

export default previewSlice.reducer;
