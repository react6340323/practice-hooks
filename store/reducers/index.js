import { combineReducers } from '@reduxjs/toolkit';
import header from 'store/reducers/header';
import movie from 'store/reducers/movie';
import preview from 'store/reducers/preview';

export default combineReducers({
  header,
  movie,
  preview,
});
