import { useRef, useState } from 'react';
import { ChevronLeft, ChevronRight, H1 } from 'tabler-icons-react';
import SliderIcon from 'components/layout/SliderIcon';

const Slider = ({ title, movies }) => {
  const rowRef = useRef(null);
  const [isMoved, setIsMoved] = useState(false);

  const handleClick = (direction) => {
    setIsMoved(true);
    if (rowRef.current) {
      const { scrollLeft, clientWidth } = rowRef.current;

      const scrollTo =
        direction === 'left'
          ? scrollLeft - clientWidth
          : scrollLeft + clientWidth;
      rowRef.current.scrollTo({ left: scrollTo, behavior: 'smooth' });
    }
  };

  return (
    <div className="slider">
      <h2 className="slider-title">
        {title}
      </h2>
      <div className="slider-div group">
        <ChevronLeft
          className={`arrow-icon left-2 ${
            !isMoved && 'hidden'
          }`}
          onClick={() => handleClick('left')}
        />
        <div
          className="card"
          ref={rowRef}
        >
          {movies.map((movie) => (
            <SliderIcon key={movie.id} movie={movie}/>
          ))}
        </div>
        <ChevronRight
          className="arrow-icon right-2"
          onClick={() => handleClick('right')}
        />
      </div>
    </div>
  );
};

export default Slider;
