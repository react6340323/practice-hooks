import Image from 'next/image';
import { useEffect, useState } from 'react';
import { useHotkeys, useHover } from '@mantine/hooks';
import ModalPreview from '../../components/layout/ModalPreview'
import { X, CirclePlus, ThumbDown, ThumbUp, PlayerPlay  } from 'tabler-icons-react'

const SliderIcon = ({ movie }) => {
  const [isOpen, setIsOpen] = useState(false);
  const { hovered, ref } = useHover();
  const [isVideoReady, setVideoReady] = useState(false);

  useEffect(() => {
      const timer1 = setTimeout(() => setVideoReady(true), 1000);
      return () => {
          clearTimeout(timer1);
      };
  }, []);

  useHotkeys([['Escape', () => setIsOpen(false)]]);

  function truncate(str, n) {
      return str?.length > n ? str.substr(0, n -1) + "..." : str;
  }

  return (
    <>
      <div className="flex" onClick={() => setIsOpen(true)} ref={ref}>
        <div className={`slider-icon ${hovered && "slider-icon-onHover"}`}>
          <div className={`top-0 left-0 -z-10 w-full `}>
            <Image
              alt=''
              src={`https://image.tmdb.org/t/p/w500${
                movie.backdrop_path || movie.poster_path
              }`}
              className="rounded-t-lg"
              height={`${hovered ? "150%" : "180%"}`}
              width="300%" 
            />
          </div>
          {
            hovered && <div className="hovered-div">
            <h5 className="hovered-title">{truncate(movie?.title || movie?.name || movie?.original_name, 30)}</h5>
            <div className="preview-overlay--button flex space-x-2 pb-[1rem]">
                <button className='hovered-play-btn'>
                    <PlayerPlay className='fill-black' size={25}/>
                </button>
                <span className="info__icon">
                    <CirclePlus />
                </span>
                <span className="info__icon">
                    <ThumbUp />
                </span>
            </div>
          </div>
          }
        </div>
      </div>

      { isOpen && <ModalPreview movie={movie} setIsOpen={setIsOpen}/> }
    </>
  );
};

export default SliderIcon;
