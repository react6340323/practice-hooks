import { X, CirclePlus, ThumbDown, ThumbUp, PlayerPlay  } from 'tabler-icons-react'
import { useEffect, useState } from 'react';
import Image from 'next/image';
import ReactPlayer from 'react-player/lazy';

const ModalPreview = ({movie, setIsOpen}) => {
	const [isVideoReady, setVideoReady] = useState(false);

	useEffect(() => {
		const timer1 = setTimeout(() => setVideoReady(true), 1000);
		return () => {
			clearTimeout(timer1);
			close();
		};
	}, []);

	const opts = {
		playerVars: {
			autoplay: 1,
			modestbranding: 1,
			controls: 0,
		},
	};

	return (
		<div className="preview-overlay">
			<div className="preview-overlay--contentBox" onClick={(e) => e.stopPropagation()}>
				<span onClick={() => setIsOpen(false)} className="preview-overlay--btnClose ">
					<X />
				</span>
				<div className="preview-overlay--videoBox">
					<div className={`${(isVideoReady && movie.trailerKey) ? "invisible" : "visible"}`}>
						<Image 
							className="preview-overlay--youtube"
							layout='fill'
							src={`https://image.tmdb.org/t/p/original${
							movie.backdrop_path || movie.poster_path
							}`}
						/>
					</div>
					{ (movie.trailerKey && isVideoReady) && <ReactPlayer
						url={`https://www.youtube.com/watch?v=${movie.trailerKey}`}
						playing={true}
						className="preview-overlay--youtube"
						width='100%'
						config={{
							youtube: {
								playerVars: { showinfo: 1 }
							},
						}}
					/>}
						
					<div className="preview-overlay--iconBox flex">
						<button className='banner-button bg-white text-black flex rounded-[5px]'>
							<PlayerPlay className='text-black mr-[5px] fill-black'/>
							Play
						</button>
						<span className="info__icon">
							<CirclePlus />
						</span>
						<span className="info__icon">
							<ThumbUp />
						</span>
						<span className="info__icon">
							<ThumbDown />
						</span>
					</div>
				</div>
				<div className="preview-overlay--text">
					<h1>{movie.title}</h1>
					<p>{movie.overview}</p>
				</div>
			</div>
		</div>
	)
}

export default ModalPreview;
