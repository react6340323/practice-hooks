import Image from 'next/image';
import { useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { setBannerMovie } from 'store/reducers/movie';
import { InfoCircle, PlayerPlay } from 'tabler-icons-react';
import { useHotkeys, useHover } from '@mantine/hooks';
import ModalPreview from '../../components/layout/ModalPreview'

const Banner = ({ movies }) => {
  const { bannerMovie } = useSelector((state) => state.movie);
  const [isOpen, setIsOpen] = useState(false);
  const dispatch = useDispatch();

  useEffect(() => {
    dispatch(
      setBannerMovie(
        movies[Math.floor(Math.random() * movies.length)]
      )
    );
  }, [dispatch, movies]);

  useHotkeys([['Escape', () => setIsOpen(false)]]);

  return (
    <div className="banner">
      <div className="banner-img">
        <Image
          alt={bannerMovie.title}
          layout="fill"
          src={`https://image.tmdb.org/t/p/original${
            bannerMovie.backdrop_path || bannerMovie.poster_path
          }`}
          objectFit="cover"
        />
      </div>

      <h1 className="banner-title">
        {bannerMovie?.title || bannerMovie?.name || bannerMovie?.original_name}
      </h1>
      <p className="overview-paragraph">{bannerMovie?.overview}</p>
      <div className="flex space-x-3">
        <button className="banner-button bg-white text-black">
          <PlayerPlay className="h-4 w-4 text-black md:h-7 md:w-7 fill-black" />
          Play
        </button>

        <button
          className="banner-button bg-[gray]/70"
          onClick={() => setIsOpen(true)}
        >
          <InfoCircle className="h-5 w-5 md:h-8 md:w-8" /> More Info
        </button>
      </div>
      { isOpen && <ModalPreview movie={bannerMovie} setIsOpen={setIsOpen}/> }
    </div>
  );
};

export default Banner;
