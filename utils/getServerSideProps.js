import { getMovies } from "request/movie";

export const getProps = async () => {
  const [
    netflixOriginals,
    trendingNow,
    topRated,
    actionMovies,
    comedyMovies,
    horrorMovies,
    romanceMovies,
    documentaries,
  ] = await Promise.all([
    getMovies('netflix-originals'),
    getMovies('trending'),
    getMovies('top-rated'),
    getMovies('action'),
    getMovies('comedy'),
    getMovies('horror'),
    getMovies('romance'),
    getMovies('documentary'),
  ]);

  return {
    props: {
      netflixOriginals,
      trendingNow,
      topRated,
      actionMovies,
      comedyMovies,
      horrorMovies,
      romanceMovies,
      documentaries,
    },
  };
};